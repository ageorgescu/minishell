/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trimmer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 12:55:00 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/12 17:04:14 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	remove_quotes(t_lst *lst)
{
	int		i;
	int		first;

	i = 0;
	first = -1;
	while (lst->content[i])
	{
		if (lst->content[i] == '\'' || lst->content[i] == '"')
		{
			if (first == -1)
				first = i;
			if (first != i && lst->content[i] == lst->content[first])
			{
				ft_memmove(&lst->content[i], &lst->content[i + 1],
					ft_strlen(&lst->content[i + 1]) + 1);
				ft_memmove(&lst->content[first], &lst->content[first + 1],
					ft_strlen(&lst->content[first + 1]) + 1);
				first = -1;
				i -= 2;
			}
		}
		i++;
	}
}

int	ft_isspace(char c)
{
	if (c == '\t'
		|| c == '\v'
		|| c == '\r'
		|| c == '\f'
		|| c == ' ')
		return (1);
	return (0);
}

void	ft_trimmer(char *str)
{
	int	i;
	int	length;

	i = 0;
	length = ft_strlen(str);
	if (!length)
		return ;
	while (length > 0 && ft_isspace(str[length - 1]))
		length--;
	while (ft_isspace(str[i]) && i < length)
		i++;
	ft_memmove(str, &str[i], length + 1 - i);
	str[length - i] = '\0';
}

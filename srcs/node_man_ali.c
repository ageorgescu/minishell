/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   node_man_ali.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/08 18:38:50 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/03/17 13:42:46 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_lst	*init_node(char *input, int size)
{
	t_lst	*node;

	node = malloc(sizeof(t_lst));
	if (!node)
		return (NULL);
	node->content = ft_substr(input, 0, size);
	if (!node->content)
	{
		free (node);
		return (NULL);
	}
	node->dis = 0;
	node->next = NULL;
	node->prev = NULL;
	return (node);
}

void	delete_node(t_lst *node)
{
	if (!node)
		return ;
	if (node->content)
		free(node->content);
	free(node);
}

t_lst	*unlink_node(t_lst *node)
{
	if (node->prev->next)
		node->prev->next = node->next;
	if (node->next)
		node->next->prev = node->prev;
	node->next = NULL;
	node->prev = NULL;
	return (node);
}

void	add_node_front(t_lst *node, t_lst **lst)
{
	if (!*lst)
	{
		*lst = node;
		(*lst)->prev = *lst;
	}
	else
	{
		node->next = *lst;
		node->prev = (*lst)->prev;
		(*lst)->prev = node;
		*lst = node;
	}
}

void	add_node_back(t_lst *node, t_lst **lst)
{
	if (!*lst)
	{
		*lst = node;
		(*lst)->prev = *lst;
	}
	else
	{
		node->prev = (*lst)->prev;
		(*lst)->prev->next = node;
		(*lst)->prev = node;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/18 16:22:17 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/18 16:22:17 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	g_error_status = 0;

int	main(int argc, char **argv, char **env)
{
	t_shell	sh;

	(void)argc;
	(void)argv;
	if (ft_initshell(env, &sh))
		return (1);
	ft_prompt(&sh);
	ft_freeshell(&sh);
	ft_close_stdfds();
	return (1);
}

/*
TODO
	fix : bash script without "#!/bin/bash" as line 1 will not execute 
		and no exit will happen in the pipe
*/

//valgrind --leak-check=yes --show-reachable=yes 
//--suppressions=readline.supp --track-fds=all ./minishell
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_list2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 15:38:18 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/19 15:38:18 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	ft_iftempenv(char **old_value, t_list **temp_env,
							t_env **data, int add)
{
	*old_value = ((t_env *)(*temp_env)->content)->value;
	if (((t_env *)(*temp_env)->content)->value && !add && *old_value)
		free(*old_value);
	if (!add)
		((t_env *)(*temp_env)->content)->value = ft_strdup((*data)->value);
	else
	{
		((t_env *)(*temp_env)->content)->value = ft_strjoin((
					(t_env *)(*temp_env)->content)->value, (*data)->value);
		if (*old_value)
			free(*old_value);
	}
}

t_list	*ft_update_env(t_list *env, t_env *data, int add)
{
	t_list	*temp_env;
	t_list	*to_add;
	char	*old_value;

	temp_env = ft_search_env(env, data->key);
	if (temp_env && data->value)
		ft_iftempenv(&old_value, &temp_env, &data, add);
	else if (!temp_env)
	{
		to_add = ft_lstnew(data);
		if (!(to_add))
		{
			ft_free_env_lst(data);
			data = NULL;
			return (NULL);
		}
		ft_lstadd_back(&env, to_add);
	}
	if (temp_env && data)
	{
		ft_free_env_lst(data);
		data = NULL;
	}
	return (env);
}

int	ft_seek_sign(char *str, int *add)
{
	int	i;

	i = 0;
	while (str[i] && str[i] != '=' && str[i] != '+')
		i++;
	if (str[i] == '+' && str[i + 1] && str[i + 1] == '=')
	{
		i++;
		(*add) = 1;
	}
	return (i);
}

t_env	*ft_separate_key_value_sup(t_env *env, char *str, int i)
{
	ft_safe_free((void **)env->key);
	env->key = ft_substr(str, 0, i);
	ft_safe_free((void **)env->value);
	env->value = ft_substr(str, i + 1, ft_strlen(str) - i - 1);
	if (!(env->key) || !(env->value))
	{
		if (env)
			free(env);
		return (NULL);
	}
	return (env);
}

t_env	*ft_separate_key_value(char *str, int *add)
{
	t_env	*env;
	int		i;

	i = ft_seek_sign(str, add);
	env = ft_calloc(1, sizeof(*env));
	if (!env)
		return (NULL);
	if (!str[i])
	{
		env->key = ft_strdup(str);
		if (!(env->key))
			return (NULL);
	}
	else
		env = ft_separate_key_value_sup(env, str, i);
	return (env);
}

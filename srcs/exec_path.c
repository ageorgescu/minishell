/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 15:14:38 by algeorge          #+#    #+#             */
/*   Updated: 2022/12/02 15:14:38 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	*get_possible_path(char *envpath, char *cmd)
{
	char	*path;
	char	*pathslash;

	if (ft_strncmp(envpath, cmd, ft_strlen(envpath)) == 0)
		path = ft_strdup(cmd);
	else
	{
		pathslash = ft_strjoin(envpath, "/");
		path = ft_strjoin(pathslash, cmd);
		free(pathslash);
	}
	return (path);
}

int	is_executable(char *cmd_path)
{
	struct stat	buf;

	if (ft_strlen(cmd_path) == 1 && ft_strcmp(cmd_path, ".") == 0)
		return (3);
	if (stat(cmd_path, &buf) != 0)
	{
		if (ft_isblank(cmd_path))
			return (5);
		return (0);
	}
	if ((buf.st_mode & S_IFMT) == S_IFDIR)
	{
		if (ft_strchr(cmd_path, '/'))
			return (4);
		return (5);
	}
	if ((buf.st_mode & S_IXUSR))
		return (1);
	if ((buf.st_mode & S_IFREG) && ft_strchr(cmd_path, '/') == 0)
		return (0);
	return (2);
}

char	*get_absolute_path(char *cmd, char *path_env)
{
	char		**all_paths;
	char		*cmd_path;
	int			i;

	i = 0;
	all_paths = ft_split(path_env, ':');
	while (all_paths && all_paths[i])
	{
		cmd_path = get_possible_path(all_paths[i], cmd);
		if (is_executable(cmd_path))
			break ;
		free(cmd_path);
		cmd_path = NULL;
		i++;
	}
	ft_free_split(all_paths, 1);
	return (cmd_path);
}

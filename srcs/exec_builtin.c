/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_builtin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 13:00:24 by algeorge          #+#    #+#             */
/*   Updated: 2023/04/11 15:54:16 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_exec_builtin_sup(t_cmd *cmd, t_shell *sh)
{
	if (ft_strcmp(cmd->argv[0], "echo") == 0)
		ft_echo(cmd);
	else if (ft_strcmp(cmd->argv[0], "pwd") == 0)
		ft_pwd(cmd, sh);
	else if (ft_strcmp(cmd->argv[0], "env") == 0)
		ft_env(cmd, sh);
	else if (ft_strcmp(cmd->argv[0], "cd") == 0)
		ft_cd(cmd, sh);
	else if (ft_strcmp(cmd->argv[0], "export") == 0)
		ft_export(cmd, sh);
	else if (ft_strcmp(cmd->argv[0], "unset") == 0)
		ft_unset(cmd, sh);
}

int	ft_exec_builtin(t_cmd *cmd, t_shell *sh, int pipe)
{
	int	pid;
	int	status;

	pid = -1;
	if (ft_strcmp(cmd->argv[0], "exit") == 0)
		cmd->status = ft_exit(cmd, sh, pipe);
	else
	{
		if (pipe)
			pid = fork();
		if (pid == 0)
		{
			close(sh->save_fd[0]);
			close(sh->save_fd[1]);
			if (cmd[1].argv)
				close(sh->old_pipe_in);
			ft_exec_builtin_sup(cmd, sh);
			status = cmd->status;
			ft_freeshell(sh);
			exit(status);
		}
		else if (!pipe)
			ft_exec_builtin_sup(cmd, sh);
	}
	return (pid);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analizer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/15 14:56:47 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/14 09:17:23 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	heredoc_expansion_flag(t_lst *next)
{
	if (next && (ft_strchr(next->content, '\"')
			|| ft_strchr(next->content, '\'')))
		return (-1);
	else
		return (-2);
}

void	identify_redir(t_lst *lst)
{
	if (lst->content[0] == '>')
	{
		if (lst->content[1])
			lst->dis = OUTPUT_APP;
		else
			lst->dis = OUTPUT;
	}
	if (lst->content[0] == '<')
	{
		if (lst->content[1])
		{
			lst->dis = HERE_DOC;
			if (lst->next)
				lst->next->dis = heredoc_expansion_flag(lst->next);
		}
		else
			lst->dis = INPUT;
	}
}

int	lex_pipe(t_lst *lst)
{
	if (lst->next && lst->prev->next
		&& lst->next->content[0] != '|' && lst->next->content[0] != '\n')
		return (0);
	error_print("minishell: syntax error near unexpected token `|'\n", NULL, 2);
	return (1);
}

int	lex_redir(t_lst *lst)
{
	if (!lst->next)
	{
		error_print("minishell: syntax error near unexpected token `newline'\n",
			NULL, 2);
		return (2);
	}
	if (lst->next->content[0] == '\n')
	{
		error_print("minishell: syntax error near unexpected token `newline'\n",
			NULL, 2);
		return (2);
	}
	if (lst->next->dis > 0)
	{
		error_print("minishell: syntax error near unexpected token `", NULL, 2);
		error_print(lst->next->content, NULL, 2);
		return (error_print("'\n", NULL, 2));
	}
	return (0);
}

int	analyzer(t_shell *shell)
{
	t_lst	*temp;

	temp = shell->tokens;
	while (temp)
	{
		if (temp->content[0] == '<' || temp->content[0] == '>')
			identify_redir(temp);
		if (temp->content[0] == '|')
			temp->dis = PIPEX;
		temp = temp->next;
	}
	temp = shell->tokens;
	while (temp)
	{
		if ((temp->dis >= OUTPUT && temp->dis <= HERE_DOC)
			&& lex_redir(temp))
			return (2);
		if (temp->dis == PIPEX && lex_pipe(temp))
			return (2);
		temp = temp->next;
	}
	return (0);
}

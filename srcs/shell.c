/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/26 14:47:56 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/26 14:47:56 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_update_shlvl(t_shell *sh)
{
	t_env	*new_shlvl;
	char	*original_value;
	char	*updated_value;

	if (!sh->env)
		return ;
	original_value = ft_get_env_value(sh->env, "SHLVL", sh);
	if (!original_value)
		return ;
	updated_value = ft_itoa(ft_atoi(original_value) + 1);
	if (!updated_value)
	{
		free(original_value);
		return ;
	}
	new_shlvl = ft_new_env_var("SHLVL", updated_value);
	ft_update_env(sh->env, new_shlvl, 0);
	free(updated_value);
}

int	ft_initshell(char **env, t_shell *sh)
{
	sh->env = ft_create_env_list(env);
	if (!sh->env)
		return (1);
	ft_update_shlvl(sh);
	sh->nbline = 0;
	sh->sinfd = -1;
	sh->soutfd = -1;
	sh->save_fd[0] = -1;
	sh->save_fd[1] = -1;
	sh->save_out = -1;
	sh->cmds = NULL;
	sh->input = NULL;
	sh->tokens = NULL;
	sh->status_str = NULL;
	return (0);
}

void	ft_freeshell(t_shell *sh)
{
	if (sh->env)
		ft_lstclear_env(&sh->env);
	if (sh->cmds)
		sh->cmds = ft_freecmd(sh->cmds);
	if (sh->sinfd != -1)
		close(sh->sinfd);
	if (sh->soutfd != -1)
		close(sh->soutfd);
	if (sh->save_fd[0] != -1)
		close(sh->save_fd[0]);
	if (sh->save_fd[1] != -1)
		close(sh->save_fd[1]);
	if (sh->status_str)
		free(sh->status_str);
	rl_clear_history();
}

void	ft_tokenlst_clear(t_lst *lst)
{
	t_lst	*cur;
	t_lst	*tmp;

	if (!lst)
		return ;
	cur = lst;
	while (cur)
	{
		tmp = cur->next;
		free(cur->content);
		free(cur);
		cur = tmp;
	}
}

t_cmd	*ft_freecmd(t_cmd *cmd)
{
	int	i;

	i = 0;
	while (cmd[i].argv)
	{
		if (cmd[i].argv[0])
			ft_free_split(cmd[i].argv, 0);
		ft_tokenlst_clear(cmd[i].redir);
		i++;
	}
	if (cmd->argv)
		free(cmd->argv);
	free(cmd);
	return (NULL);
}

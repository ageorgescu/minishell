/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_constr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 09:53:39 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/03/17 09:25:30 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	cmd_counter(t_lst *lst)
{
	int	i;

	i = 0;
	while (lst)
	{
		if (lst->dis == 0 || lst->dis == 6)
			i++;
		lst = lst->next;
	}
	return (i);
}

int	cmd_block_counter(t_lst *lst)
{
	int	i;

	i = 1;
	while (lst)
	{
		if (lst->content[0] == '|')
			i++;
		lst = lst->next;
	}
	return (i);
}

void	make_redir(t_cmd *cmd, t_lst **lst)
{
	t_lst	*temp;

	temp = *lst;
	*lst = (*lst)->next;
	add_node_back(unlink_node(temp), &cmd->redir);
	temp = *lst;
	*lst = (*lst)->next;
	add_node_back(unlink_node(temp), &cmd->redir);
}

char	**make_argv(int i, char **strs, t_shell *shell)
{
	t_lst	*temp;

	while (shell->tokens)
	{
		temp = shell->tokens;
		shell->tokens = shell->tokens->next;
		if (temp->dis == 0)
		{
			*strs = temp->content;
			strs++;
			free(unlink_node(temp));
		}
		else if (OUTPUT <= temp->dis && temp->dis <= HERE_DOC)
		{
			shell->tokens = shell->tokens->next;
			make_redir(&(shell->cmds[i]), &(temp));
		}
		else if (temp->dis == 6)
		{
			delete_node(unlink_node(temp));
			break ;
		}
	}
	*strs = NULL;
	return (++strs);
}

int	reconstructor(t_shell *shell)
{
	char	**strs;
	int		i;
	int		nbr;

	i = 0;
	nbr = cmd_block_counter(shell->tokens);
	shell->cmds = ft_calloc(1, sizeof(t_cmd) * (nbr + 1));
	if (!shell->cmds)
		return (error_print("Error, memory error\n", shell->tokens, 1));
	strs = ft_calloc(1, sizeof(char *) * (cmd_counter(shell->tokens) + 1));
	if (!strs)
	{
		shell->cmds = ft_freecmd(shell->cmds);
		return (error_print("Error, memory error\n", shell->tokens, 1));
	}
	while (i < nbr)
	{
		shell->cmds[i].argv = strs;
		shell->cmds[i].redir = NULL;
		shell->cmds[i].pid = -1;
		strs = make_argv(i, strs, shell);
		i++;
	}
	shell->cmds[i].argv = NULL;
	return (0);
}

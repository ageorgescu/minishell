/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 14:35:09 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/19 14:35:09 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_safe_free(void **ptr)
{
	if (ptr && *ptr)
	{
		free(*ptr);
		*ptr = NULL;
	}
}

int	ft_is_escaped(char *str, int pos)
{
	int	i;
	int	bkslh;

	i = 0;
	bkslh = 0;
	while (str[i])
	{
		if (str[i] != '\\')
			bkslh = 0;
		if (str[i] == '\\')
			bkslh++;
		i++;
		if (i == pos)
			return (bkslh % 2 != 0);
	}
	return (bkslh % 2 != 0);
}

int	is_builtin(char *value)
{
	if (!value)
		return (0);
	if (!(ft_strcmp(value, "echo")) || !(ft_strcmp(value, "cd")))
		return (1);
	if (!(ft_strcmp(value, "pwd")) || !(ft_strcmp(value, "export")))
		return (1);
	if (!(ft_strcmp(value, "unset")) || !(ft_strcmp(value, "env")))
		return (1);
	if (!(ft_strcmp(value, "exit")))
		return (1);
	return (0);
}

void	ft_create_pipe(int *old_pipe_in, int next)
{
	int	new_pipe[2];

	dup2(*old_pipe_in, 0);
	if (*old_pipe_in != 0)
		close(*old_pipe_in);
	if (!next)
		return ;
	pipe(new_pipe);
	dup2(new_pipe[1], 1);
	close(new_pipe[1]);
	*old_pipe_in = new_pipe[0];
}

void	ft_close_stdfds(void)
{
	close(0);
	close(1);
	close(2);
}

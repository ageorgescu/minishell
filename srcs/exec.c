/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 13:00:24 by algeorge          #+#    #+#             */
/*   Updated: 2023/04/07 14:47:27 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_exec_cmd(t_cmd *cmd, t_shell *sh)
{
	int		pid;
	char	**env;
	char	*cmd_wpath;

	if (!(cmd->argv[0]) || (!add_path_cmd(cmd, cmd->argv[0], &cmd_wpath, sh)))
		return (-1);
	pid = fork();
	ft_exec_signals();
	if (pid == 0)
	{
		close(sh->save_fd[0]);
		close(sh->save_fd[1]);
		if (cmd[1].argv)
			close(sh->old_pipe_in);
		env = ft_lst_env_to_split_export(sh->env, 0);
		execve(cmd_wpath, cmd->argv, env);
		ft_free_split(env, 1);
	}
	free(cmd_wpath);
	return (pid);
}

int	ft_exec_pid(t_cmd *cmd, t_shell *sh, int pipe)
{
	if (is_builtin(cmd->argv[0]))
		return (ft_exec_builtin(cmd, sh, pipe));
	else
		return (ft_exec_cmd(cmd, sh));
}

void	ft_waitpid(t_cmd *cmd, t_shell *sh)
{
	int	status;
	int	i;

	i = 0;
	status = 127;
	if (cmd[0].argv)
	{
		while (cmd[i].argv)
		{
			if (cmd[i].pid != -1)
			{
				waitpid(cmd[i].pid, &status, 0);
				if (WIFEXITED(status))
					cmd[i].status = WEXITSTATUS(status);
				else if (g_error_status == 130 || g_error_status == 131)
					cmd[i].status = g_error_status;
			}
			i++;
		}
		g_error_status = cmd[i - 1].status;
	}
	else
		fprintf(stderr, "what = %d\n", g_error_status);
	if (sh->old_pipe_in != 0)
		close(sh->old_pipe_in);
}

void	ft_exec(t_cmd *cmd, t_shell *sh)
{
	int	i;
	int	redir;

	i = 0;
	sh->old_pipe_in = 0;
	while (cmd[i].argv)
	{
		ft_save_stdfds(sh->save_fd);
		ft_create_pipe(&(sh->old_pipe_in), (cmd[i + 1].argv != NULL));
		redir = redirections(&cmd[i], sh);
		if (redir == 1)
		{
			ft_restore_stdfds(sh->save_fd);
			g_error_status = cmd[i].status;
			if (sh->old_pipe_in != 0)
				close(sh->old_pipe_in);
			return ;
		}
		if (redir != 2)
			cmd[i].pid = ft_exec_pid(&cmd[i], sh, (cmd[1].argv != NULL));
		ft_restore_stdfds(sh->save_fd);
		i++;
	}
	ft_waitpid(cmd, sh);
}

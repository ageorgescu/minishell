/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/15 01:29:16 by algeorge          #+#    #+#             */
/*   Updated: 2022/11/15 01:29:16 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

long long	ft_atoll(char *nptr)
{
	long long	sign;
	int			i;
	long long	n;

	i = 0;
	sign = 1;
	n = 0;
	while (nptr[i] == ' ' || nptr[i] == '\n' || nptr[i] == '\t'
		|| nptr[i] == '\v' || nptr[i] == '\f' || nptr[i] == '\r')
		i++;
	if (nptr[i] == '-' || nptr[i] == '+')
	{
		if (nptr[i] == '-')
			sign *= -1;
		i++;
	}
	while (nptr[i] >= '0' && nptr[i] <= '9')
	{
		n = n * 10 + (nptr[i] - '0');
		i++;
	}
	return (n * sign);
}

int	ft_isnumber(char *str)
{
	int	i;

	i = 0;
	if (!str)
		return (0);
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i] == '0')
		i++;
	if (ft_strlen(str + i) > 19)
		return (0);
	if (!ft_isalldigit(str + i))
		return (0);
	if (str[0] == '-')
		return (((unsigned long long)ft_atoll(str + i) <= 9223372036854775807)
			|| (((unsigned long long)ft_atoll(str + i) > 9223372036854775807)
				&& !ft_strcmp(str + i, "9223372036854775808")));
	else if ((unsigned long long)ft_atoll(str) > 9223372036854775807)
		return (0);
	return (1);
}

int	ft_count_split(char **cmd)
{
	int	i;

	i = 0;
	if (!cmd)
		return (0);
	while (cmd[i])
		i++;
	return (i);
}

int	get_exitcode(t_cmd *cmd)
{
	if (ft_isnumber(cmd->argv[1]))
		return (ft_atoll(cmd->argv[1]));
	else
	{
		ft_sherror(cmd, cmd->argv[1], "numeric argument required");
		return (2);
	}
}

int	ft_exit(t_cmd *cmd, t_shell *sh, int pipe)
{
	int	exitcode;
	int	nb_arg;

	cmd->status = 0;
	exitcode = 0;
	if (cmd)
	{
		nb_arg = ft_count_split(cmd->argv);
		if (nb_arg == 2)
			exitcode = get_exitcode(cmd);
		else if (nb_arg > 2)
			return (ft_error(cmd, "minishell: exit: too many arguments"));
		ft_putstr_fd("exit\n", 1);
	}
	if (!pipe)
	{
		if (sh)
			ft_freeshell(sh);
		ft_close_stdfds();
		exit(exitcode);
	}
	return (exitcode % 256);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 14:30:13 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/24 14:30:13 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_error(t_cmd *cmd, char *error)
{
	ft_putendl_fd(error, 2);
	cmd->status = 1;
	return (1);
}

void	ft_enverror(t_cmd *cmd)
{
	ft_putstr_fd("env: '", 2);
	ft_putstr_fd(cmd->argv[1], 2);
	ft_putendl_fd("': No such file or directory", 2);
}

int	ft_sherror(t_cmd *cmd, char *arg, char *str)
{
	ft_putstr_fd("minishell: ", 2);
	if (cmd->argv[0])
	{
		ft_putstr_fd(cmd->argv[0], 2);
		ft_putstr_fd(": ", 2);
	}
	if (arg)
	{
		ft_putstr_fd("`", 2);
		ft_putstr_fd(arg, 2);
		ft_putstr_fd("': ", 2);
	}
	if (str)
		ft_putendl_fd(str, 2);
	else
		ft_putendl_fd("", 2);
	cmd->status = 1;
	return (1);
}

void	ft_unseterror(t_cmd *cmd, char *arg)
{
	ft_putstr_fd("minishell: unset: `", 2);
	ft_putstr_fd(arg, 2);
	ft_putendl_fd("': not a valid identifier", 2);
	cmd->status = 1;
}

void	ft_heredoc_warning(char *eof, int nbline, int tmp_fd, int old_pipe_in)
{
	ft_putstr_fd("minishell: warning: here-document at line ", 2);
	ft_putnbr_fd(nbline, 2);
	ft_putstr_fd(" delimited by end-of-file (wanted '", 2);
	ft_putstr_fd(eof, 2);
	ft_putendl_fd("')", 2);
	free(eof);
	close(tmp_fd);
	close(old_pipe_in);
	g_error_status = 0;
	close(0);
	close(1);
	close(2);
	exit(0);
}

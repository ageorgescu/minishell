/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 14:08:55 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/24 14:08:55 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_export_check_underscore(char *str)
{
	if (!ft_strncmp(str, "_=", 2) || !ft_strncmp(str, "_", 2))
		return (1);
	return (0);
}

int	ft_export_check_arg(char *arg)
{
	int	i;

	i = 0;
	if (ft_export_check_underscore(arg))
		return (0);
	if (!ft_strlen(arg))
		return (1);
	while (arg[i])
	{
		if (i == 0 && !ft_isalpha(arg[i]) && arg[i] != '_')
			return (1);
		else if (!ft_isalnum(arg[i]))
		{
			if (arg[i] == '+' && arg[i + 1] && arg[i + 1] != '=')
				return (1);
			else if (arg[i] == '=')
				return (0);
			else if (arg[i] != '_')
				return (1);
		}
		i++;
	}
	return (0);
}

static t_list	*ft_export_update_env(t_cmd *cmd, t_list *env, char *arg)
{
	t_env	*result;
	t_list	*tmp;
	int		add;

	add = 0;
	result = NULL;
	if (ft_export_check_underscore(arg))
		return (env);
	result = ft_separate_key_value(arg, &add);
	tmp = ft_update_env(env, result, add);
	if (!tmp)
		cmd->status = 1;
	return (tmp);
}

int	ft_export(t_cmd *cmd, t_shell *sh)
{
	int		i;
	char	**env;

	cmd->status = 0;
	i = (cmd->argv[1] == NULL) * -1;
	if (!cmd->argv[1])
	{
		env = ft_lst_env_to_split_export(sh->env, 1);
		if (!(env))
			return (1);
		ft_strs_sort(env, ft_lstsize(sh->env));
		while (env[++i])
			if (ft_strncmp(env[i], "_=", 2))
				printf("declare -x %s\n", env[i]);
		ft_free_split(env, 1);
		return (0);
	}
	while (cmd->argv[++i])
	{
		if (ft_export_check_arg(cmd->argv[i]))
			ft_sherror(cmd, cmd->argv[i], "not a valid identifier");
		else if (!(ft_export_update_env(cmd, sh->env, cmd->argv[i])))
			return (1);
	}
	return (0);
}

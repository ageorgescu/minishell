/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_list3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 14:44:23 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/24 14:44:23 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	ft_concat_value(char *result, char *str, int export)
{
	ft_strlcat(result, "=", ft_strlen(result) + 2);
	if (export)
		ft_strlcat(result, "\"", ft_strlen(result) + 2);
	ft_strlcat(result, str, ft_strlen(result) + ft_strlen(str) + 1);
	if (export)
		ft_strlcat(result, "\"", ft_strlen(result) + 2);
}

char	**ft_lst_env_to_split_export(t_list *lst, int export)
{
	t_env	*content;
	char	**result;
	int		i;

	result = ft_calloc(ft_lstsize(lst) + 1, sizeof(*result));
	if (!(result))
		return (NULL);
	i = 0;
	while (lst)
	{
		content = (t_env *)lst->content;
		result[i] = ft_calloc((ft_strlen(content->key)
					+ ft_strlen(content->value) + 4), sizeof(char));
		if (!(result[i]) && !(ft_free_split(result, 1)))
			return (NULL);
		ft_strlcat(result[i], content->key, ft_strlen(content->key) + 1);
		if (content->value)
			ft_concat_value(result[i], content->value, export);
		i++;
		lst = lst->next;
	}
	return (result);
}

char	*ft_get_status_str(t_shell *sh)
{
	if (sh->status_str)
		free(sh->status_str);
	sh->status_str = ft_itoa(g_error_status);
	return (sh->status_str);
}

int	get_path_var(char **path, t_shell *sh)
{
	*path = ft_get_env_value(sh->env, "PATH", sh);
	if (!*path)
	{
		g_error_status = 127;
		return (0);
	}
	return (1);
}

char	*ft_get_env_value(t_list *lst_env, char *key, t_shell *sh)
{
	t_env	*env;
	t_list	*result;

	if (key[0] == '?')
		return (ft_get_status_str(sh));
	result = ft_search_env(lst_env, key);
	if (!result)
		return (NULL);
	env = (t_env *)(result->content);
	if (!env)
		return (NULL);
	return (env->value);
}

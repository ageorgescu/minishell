/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 14:54:07 by algeorge          #+#    #+#             */
/*   Updated: 2022/12/14 14:54:07 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	clear_tmp_file_input(t_cmd *cmd)
{
	int		tmp_fd;

	tmp_fd = open("/tmp/tempfile", O_WRONLY | O_TRUNC, 0600);
	if (tmp_fd != -1)
		close(tmp_fd);
	cmd->status = 130;
}

void	make_tmp_file_input(void)
{
	int		tmp_fd;

	tmp_fd = open("/tmp/tempfile", O_RDONLY);
	unlink("/tmp/tempfile");
	if (tmp_fd != -1)
		dup2close(&tmp_fd, 0);
}

int	flush_input_fd(int pid, t_shell *sh, int tmp_fd)
{
	dup2(sh->save_fd[0], 0);
	close(sh->save_fd[0]);
	sh->save_fd[0] = dup(0);
	if (pid == 0)
	{
		if (sh->save_fd[0] != -1)
			sh->save_out = dup(1);
		dup2(sh->save_fd[1], 1);
		close(sh->save_fd[1]);
	}
	if (sh->save_fd[0] == -1 && pid == 0)
	{
		close(sh->old_pipe_in);
		g_error_status = tmp_fd;
		ft_heredoc_int(SIGINT);
	}
	return (sh->save_fd[0] == -1);
}

void	get_and_write_input(int tmp_fd, char *eof, t_shell *sh, int nbline)
{
	char	*input;

	close(sh->save_out);
	g_error_status = sh->old_pipe_in;
	ft_freeshell(sh);
	signal(SIGINT, ft_heredoc_int);
	while (1)
	{
		input = readline("> ");
		if (!input)
			ft_heredoc_warning(eof, nbline, tmp_fd, sh->old_pipe_in);
		if (ft_strcmp(input, eof))
			ft_putendl_fd(input, tmp_fd);
		else
		{
			free(input);
			break ;
		}
		free(input);
	}
	free(eof);
	ft_close_stdfds();
	close(tmp_fd);
	close(sh->old_pipe_in);
	exit(0);
}

int	ft_heredoc(t_cmd *cmd, char *eof, int flag, t_shell *sh)
{
	int		tmp_fd;
	int		pid;
	int		status;
	char	*eof2;

	tmp_fd = open("/tmp/tempfile", O_WRONLY | O_CREAT | O_TRUNC, 0600);
	if (tmp_fd == -1)
		return (1);
	signal(SIGINT, SIG_IGN);
	eof2 = ft_strdup(eof);
	pid = fork();
	flush_input_fd(pid, sh, sh->old_pipe_in);
	if (pid == 0)
		get_and_write_input(tmp_fd, eof2, sh, sh->nbline);
	waitpid(pid, &status, 0);
	close(tmp_fd);
	if (WIFEXITED(status) && WEXITSTATUS(status) == 130)
		clear_tmp_file_input(cmd);
	make_tmp_file_input();
	free(eof2);
	if (flag != -1)
		return (ft_heredoc_expansion(0, sh->env, sh, cmd));
	return (0);
}

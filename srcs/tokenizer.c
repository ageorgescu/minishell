/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 12:55:00 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/04 07:37:26 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	clear_empty(t_lst *lst)
{
	t_lst	*temp;

	temp = lst;
	while (temp)
	{
		ft_trimmer(temp->content);
		if (ft_strlen(temp->content) == 0)
		{
			if (temp == lst)
				lst = lst->next;
			temp = temp->next;
			delete_node(unlink_node(temp->prev));
		}
		else
			temp = temp->next;
	}
	return (0);
}

int	make_token(char *input, int index, t_shell *shell)
{
	t_lst	*node;

	node = init_node(input, index);
	if (!node)
		return (1);
	node->dis = 0;
	add_node_back(node, &(shell->tokens));
	ft_memmove(input, &input[index], ft_strlen(&input[index]) + 1);
	return (0);
}

int	add_separator(char *input, t_shell *shell)
{
	int	size;

	size = 1;
	if (!input[0])
		return (0);
	if ((input[0] == '<' || input[0] == '>')
		&& input[0] == input[1])
		size++;
	if (make_token(input, size, shell))
		return (1);
	return (0);
}

char	set_qoutes(int flag, char c)
{
	if (flag == 0 && (c == '\'' || c == '"'))
		flag = c;
	else
		if (flag == c)
			flag = 0;
	return (flag);
}

int	tokenize(char *input, t_shell *shell)
{
	int		i;
	char	flag;

	i = 0;
	flag = 0;
	while (i < (int)ft_strlen(input))
	{
		ft_trimmer(input);
		flag = set_qoutes(flag, input[i]);
		if (flag == 0 && (ft_strchr("| ><\n", input[i])))
		{
			if (i != 0)
				if (make_token(input, i, shell) == 1)
					return (1);
			if (add_separator(input, shell) == 1)
				return (1);
			i = -1;
		}
		i++;
	}
	if (i != 0 && make_token(input, i, shell) == 1)
		return (1);
	clear_empty(shell->tokens);
	return (0);
}

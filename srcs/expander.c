/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expander.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 12:49:36 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/12 17:04:09 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ft_replace(char *src, int index, int size, char *replace)
{
	int		length;
	char	*result;

	length = ft_strlen(replace);
	result = malloc(sizeof(char) * (ft_strlen(src) - size + length + 1));
	if (!result)
	{
		free(src);
		return (NULL);
	}
	result[0] = '\0';
	ft_memcpy(result, src, index);
	if (replace)
		ft_memcpy(&result[index], replace, length + 1);
	ft_memcpy(&result [index + length], &src[index + size],
		ft_strlen(&src[index + size]) + 1);
	free(src);
	return (result);
}

void	preprocess(char *str, char q, char s, int c)
{
	int	i;
	int	flag;

	flag = -1;
	i = 0;
	while (str[i])
	{
		if (flag == -1 && str[i] == q)
			flag = q;
		else
			if (flag == str[i])
				flag = -1;
		if (flag != -1 && str[i] == c * s)
			str[i] = -1 * str[i];
		i++;
	}
}

int	get_word_size(char *str)
{
	int	i;

	i = 1;
	while (ft_isalnum(str[i]) || str[i] == '_')
		i++;
	return (i);
}

char	*expansion(char *temp, t_list *env_list, t_shell *sh)
{
	char	*var_name;
	int		i;

	i = 0;
	while (temp[i])
	{
		if (temp[i] == '$' && (temp[i + 1] == '?' || temp[i + 1] == '_'
				|| ft_isalpha(temp[i + 1])))
		{
			if (temp[i + 1] == '?')
				var_name = ft_strdup("?");
			else
				var_name = ft_substr(temp, i + 1, get_word_size(&temp[i + 1]));
			if (!var_name)
				return (NULL);
			temp = ft_replace(temp, i, ft_strlen(var_name) + 1,
					ft_get_env_value(env_list, var_name, sh));
			i += ft_strlen(ft_get_env_value(env_list, var_name, sh)) - 1;
			free(var_name);
			if (!temp)
				return (NULL);
		}
		i++;
	}
	return (temp);
}

int	expansions(t_shell *shell, t_list *env_list)
{
	t_lst	*temp;

	temp = shell->tokens;
	while (temp)
	{
		if (temp->dis >= 0)
		{
			preprocess(temp->content, '"', '\'', 1);
			preprocess(temp->content, '\'', '$', 1);
			temp->content = expansion(temp->content, env_list, shell);
			if (!temp->content)
				return (1);
			preprocess(temp->content, '"', '\'', -1);
			preprocess(temp->content, '\'', '$', -1);
		}
		remove_quotes(temp);
		temp = temp->next;
	}
	return (0);
}

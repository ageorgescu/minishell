/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 14:14:28 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/07 12:11:19 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	err_out(t_cmd *cmd, char *msg1, char *msg2, char *msg3)
{
	write(2, msg1, ft_strlen(msg1));
	write(2, msg2, ft_strlen(msg2));
	write(2, msg3, ft_strlen(msg3));
	cmd->status = 1;
	return (1);
}

int	dup2close(int *source, int target)
{
	if (*source != -1)
	{
		if (dup2(*source, target) == -1)
			return (1);
		if (close(*source) == -1)
			return (1);
		*source = -1;
	}
	else
		return (1);
	return (0);
}

int	get_infile(t_cmd *cmd, char *infile)
{
	int	infd;

	infd = -1;
	if (access(infile, F_OK))
		return (err_out(cmd, "minishell: ",
				infile, ": No such file or directory\n"));
	else if (access(infile, R_OK))
		return (err_out(cmd, "minishell: ", infile, ": Permission denied\n"));
	else
	{
		infd = open(infile, O_RDONLY);
		if (dup2close(&infd, 0))
			return (err_out(cmd, "minishell: ", infile, ": file error\n"));
	}
	return (0);
}

int	get_outfile(t_cmd *cmd, char *outfile, int append)
{
	int	outfd;

	outfd = -1;
	if (!access(outfile, F_OK) && access(outfile, W_OK))
		return (err_out(cmd, "minishell: ", outfile, ": Permission denied\n"));
	if (append)
		outfd = open(outfile, O_CREAT | O_WRONLY | O_APPEND, 0644);
	else
		outfd = open(outfile, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (dup2close(&outfd, 1))
	{
		if (access(outfile, F_OK))
			return (err_out(cmd, "minishell: ",
					outfile, ": file creation error\n"));
		else
			return (err_out(cmd, "minishell: ", outfile, ": open() error\n"));
	}
	return (0);
}

int	redirections(t_cmd *cmd, t_shell *shell)
{
	t_lst	*temp;

	temp = cmd->redir;
	while (temp)
	{
		if (temp->dis == OUTPUT)
			if (get_outfile(cmd, temp->next->content, 0))
				return (2);
		if (temp->dis == OUTPUT_APP)
			if (get_outfile(cmd, temp->next->content, 1))
				return (2);
		if (temp->dis == INPUT)
			if (get_infile(cmd, temp->next->content))
				return (2);
		if (temp->dis == HERE_DOC)
			if (ft_heredoc(cmd, temp->next->content,
					temp->next->dis, shell))
				return (1);
		temp = temp->next;
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 14:03:50 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/19 14:03:50 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_pwd(t_cmd *cmd, t_shell *sh)
{
	char	*cwd;
	char	*value;
	t_list	*env;

	env = sh->env;
	cmd->status = 0;
	env = ft_search_env(env, "PWD");
	if (env)
		value = ((t_env *)(env->content))->value;
	cwd = ft_getcwd();
	if (!cwd)
		cwd = ft_strdup(value);
	printf("%s\n", cwd);
	free(cwd);
	return (0);
}

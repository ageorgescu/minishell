/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 14:35:09 by algeorge          #+#    #+#             */
/*   Updated: 2023/04/07 15:13:26 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_isalldigit(char *str)
{
	while (*str)
		if (!ft_isdigit(*str++))
			return (0);
	return (1);
}

int	ft_isblank(char *str)
{
	int	i;

	i = 0;
	if (ft_strlen(str) == 0)
		return (1);
	while (str[i])
	{
		if (str[i] == ' ' || str[i] == '\n' || str[i] == '\t'
			|| str[i] == '\v' || str[i] == '\f' || str[i] == '\r')
			i++;
		else
			return (0);
	}
	return (1);
}

int	set_error_status(t_cmd *cmd, char *cmdname, int exec_status)
{
	if (exec_status == 3)
	{
		err_out(cmd, "minishell: ", cmdname,
			": filename argument required\n.: usage: . filename [arguments]\n");
		cmd->status = 2;
	}
	if (exec_status == 0 || exec_status == 5)
	{
		if (!ft_strchr(cmdname, '/') || !ft_strlen(cmdname))
			err_out(cmd, "", cmdname, ": command not found\n");
		else
			err_out(cmd, "minishell: ",
				cmdname, ": No such file or directory\n");
		cmd->status = 127;
	}
	if (exec_status == 2 || exec_status == 4)
	{
		if (exec_status == 2)
			err_out(cmd, "minishell: ", cmdname, ": Permission denied\n");
		else if (exec_status == 4)
			err_out(cmd, "minishell: ", cmdname, ": Is a directory\n");
		cmd->status = 126;
	}
	return (0);
}

int	add_path_cmd(t_cmd *cmd, char *cmdname, char **cmd_wpath, t_shell *sh)
{
	char	*path;
	int		exec_status;

	if (!cmdname)
		return (0);
	exec_status = is_executable(cmdname);
	if (exec_status == 1)
	{
		*cmd_wpath = ft_strdup(cmdname);
		return (1);
	}
	else if (exec_status >= 2)
		return (set_error_status(cmd, cmdname, exec_status));
	get_path_var(&path, sh);
	if (path)
		*cmd_wpath = get_absolute_path(cmdname, path);
	else
		*cmd_wpath = ft_strdup(cmdname);
	exec_status = is_executable(*cmd_wpath);
	if (exec_status == 0 || exec_status >= 2)
	{
		free(*cmd_wpath);
		return (set_error_status(cmd, cmdname, exec_status));
	}
	return (1);
}

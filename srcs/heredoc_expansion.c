/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_expansion.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 07:39:50 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/04 12:21:42 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_heredoc_expansion(int fd, t_list *env_lst, t_shell *sh, t_cmd *cmd)
{
	char	*line;
	char	*exp_line;
	int		tmp_fd;

	if (cmd->status == 130)
		return (1);
	tmp_fd = open("/tmp/tempfile", O_WRONLY | O_CREAT | O_TRUNC, 0600);
	if (tmp_fd == -1)
		return (1);
	line = get_next_line(fd);
	while (line)
	{
		exp_line = expansion(line, env_lst, sh);
		if (exp_line)
		{
			ft_putstr_fd(exp_line, tmp_fd);
			free(exp_line);
		}
		line = get_next_line(fd);
	}
	close(tmp_fd);
	make_tmp_file_input();
	return (0);
}

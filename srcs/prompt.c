/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/06 13:01:21 by algeorge          #+#    #+#             */
/*   Updated: 2023/03/17 09:08:32 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_save_history(char *input)
{
	if (input && *input)
		add_history(input);
}

char	*ft_create_prompt(void)
{
	char	*cwd;
	char	*name;
	char	*buffer;
	char	*prompt;

	buffer = NULL;
	cwd = getcwd(buffer, 0);
	name = ft_strjoin("\e[1;32mminishell\e[0m:\e[1;34m", cwd);
	prompt = ft_strjoin(name, "\e[0m$ ");
	free(cwd);
	free(name);
	free(buffer);
	return (prompt);
}

char	*ft_read_input(char *input)
{
	ft_input_signals();
	input = readline("\e[1;32mminishell\e[0m$ ");
	if (!input)
		input = ft_strdup("exit");
	return (input);
}

int	ft_prompt(t_shell *sh)
{
	int		pars;

	while (1)
	{
		sh->input = NULL;
		(sh->nbline)++;
		sh->tokens = NULL;
		sh->input = ft_read_input(sh->input);
		if (!sh->input)
			return (error_print("Memory error\n", NULL, 1));
		ft_save_history(sh->input);
		pars = parsing(sh);
		free(sh->input);
		if (pars == 1)
			return (1);
		if (pars == 2)
			g_error_status = 2;
		else if (sh->tokens)
		{
			if (reconstructor(sh))
				return (1);
			ft_exec(sh->cmds, sh);
			sh->cmds = ft_freecmd(sh->cmds);
		}
	}
}

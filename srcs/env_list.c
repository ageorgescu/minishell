/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_list.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 14:30:32 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/19 14:30:32 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_free_env_lst(void *env_lst)
{
	t_env	*temp;

	if (!env_lst)
		return ;
	temp = (t_env *)env_lst;
	if (temp->key)
		free(temp->key);
	if (temp->value)
		free(temp->value);
	free(temp);
}

void	ft_lstclear_env(t_list **env)
{
	t_list	*cur;
	t_list	*tmp;

	if (!env || !(*env))
		return ;
	cur = *env;
	while (cur)
	{
		tmp = cur->next;
		ft_free_env_lst(cur->content);
		free(cur);
		cur = tmp;
	}
}

t_list	*ft_search_env(t_list *env_list, char *arg)
{
	t_env	*env;
	t_env	*env_content;
	t_list	*temp_list;

	temp_list = env_list;
	env = ft_separate_key_value(arg, 0);
	if (!env)
		return (NULL);
	while (temp_list)
	{
		env_content = (t_env *)temp_list->content;
		if (ft_strcmp(env_content->key, env->key) == 0)
		{
			ft_free_env_lst(env);
			return (temp_list);
		}
		temp_list = temp_list->next;
	}
	ft_free_env_lst(env);
	return (NULL);
}

t_list	*ft_create_env_list(char **env)
{
	t_list	*env_list;
	t_list	*tmp;
	int		i;

	i = 0;
	env_list = NULL;
	while (env[i])
	{
		if (ft_strncmp(env[i], "LD_PRELOAD", 10))
		{
			tmp = ft_lstnew(ft_separate_key_value(env[i], 0));
			if (!tmp)
			{
				ft_lstclear_env(&env_list);
				return (NULL);
			}
			if (i == 0)
				env_list = tmp;
			else
				ft_lstadd_back(&env_list, tmp);
		}
		i++;
	}
	return (env_list);
}

t_env	*ft_new_env_var(char *key, char *value)
{
	t_env	*result;

	result = ft_calloc(1, sizeof(*result));
	if (!result)
		return (NULL);
	result->key = ft_strdup(key);
	if (!(result->key))
	{
		ft_free_env_lst(result);
		return (NULL);
	}
	result->value = ft_strdup(value);
	if (!(result->value))
	{
		ft_free_env_lst(result);
		return (NULL);
	}
	return (result);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/18 16:57:34 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/18 16:57:34 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_isalln(char *str)
{
	int	i;

	if (!str || str[0] != '-')
		return (0);
	i = 1;
	while (str[i] == 'n' && i++)
		;
	if (str[i])
		return (0);
	return (1);
}

int	ft_echo(t_cmd *cmd)
{
	int		i;
	int		n;

	n = 0;
	i = 1;
	cmd->status = 0;
	while (cmd->argv[i] && ft_isalln(cmd->argv[i]) && (i++))
		n = 1;
	while (cmd->argv[i])
	{
		printf("%s", cmd->argv[i]);
		i++;
		if (cmd->argv[i])
			printf(" ");
	}
	if (n == 0)
		printf("\n");
	return (0);
}

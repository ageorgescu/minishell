/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 11:22:46 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/24 11:22:46 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	ft_env(t_cmd *cmd, t_shell *sh)
{
	t_list	*temp;
	t_env	*content;

	temp = sh->env;
	cmd->status = 0;
	if (cmd->argv[1])
	{
		cmd->status = 1;
		ft_enverror(cmd);
		return (1);
	}
	while (temp)
	{
		content = (t_env *)temp->content;
		if (content->value)
		{
			ft_putstr_fd(content->key, 1);
			ft_putstr_fd("=", 1);
			ft_putendl_fd(content->value, 1);
		}
		temp = temp->next;
	}
	return (0);
}

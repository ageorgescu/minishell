/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 14:26:32 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/27 14:26:32 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_unset_check_arg(char *arg)
{
	int	i;

	i = 0;
	while (arg[i])
	{
		if (i == 0 && !ft_isalpha(arg[i]))
			return (1);
		else if (!(ft_isalnum(arg[i]) || arg[i] == '_')
			&& !ft_is_escaped(arg, i))
			return (1);
		i++;
	}
	return (0);
}

int	ft_unset(t_cmd *cmd, t_shell *sh)
{
	int		i;
	t_list	*to_erase;

	i = 1;
	cmd->status = 0;
	while (cmd->argv[i])
	{
		if (!ft_unset_check_arg(cmd->argv[i]))
		{
			to_erase = ft_search_env(sh->env, cmd->argv[i]);
			if (to_erase)
				ft_lstdel_middle(&sh->env, to_erase->content, ft_free_env_lst);
		}
		i++;
	}
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd2.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 11:40:12 by algeorge          #+#    #+#             */
/*   Updated: 2022/11/01 11:40:12 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ft_getcwd(void)
{
	char	*working_var;
	char	*result;

	working_var = NULL;
	working_var = getcwd(working_var, 0);
	result = ft_strdup(working_var);
	if (!working_var)
		return (NULL);
	else if (!result)
	{
		free(working_var);
		return (NULL);
	}
	free(working_var);
	return (result);
}

t_list	*ft_change_dir_update(t_list *env, char *oldpwd)
{
	t_env	*oldpwd_env;
	t_env	*pwd_env;
	char	*pwd;

	pwd = NULL;
	oldpwd_env = NULL;
	pwd_env = NULL;
	pwd = ft_getcwd();
	if (pwd)
		oldpwd_env = ft_new_env_var("OLDPWD", oldpwd);
	if (oldpwd_env)
		ft_update_env(env, oldpwd_env, 0);
	if (pwd)
		pwd_env = ft_new_env_var("PWD", pwd);
	else
		pwd_env = ft_new_env_var("PWD", oldpwd);
	if (pwd_env)
		ft_update_env(env, pwd_env, 0);
	free(pwd);
	return (env);
}

int	ft_freetmppwd(char **oldpwd, char **newpwd)
{
	if (*oldpwd)
		free(*oldpwd);
	if (*newpwd)
		free(*newpwd);
	return (1);
}

int	ft_cdparenterror(t_cmd *cmd)
{
	ft_putstr_fd("cd: error retrieving current directory:", 2);
	ft_putstr_fd(" getcwd: cannot access parent directories:", 2);
	ft_putstr_fd(" No such file or directory\n", 2);
	cmd->status = 1;
	return (0);
}

int	ft_chdir(int chdir_value, char *oldpwd, t_cmd *cmd, t_shell *sh)
{
	char	*cwd;

	if (chdir_value == -1 && oldpwd)
		return (ft_sherror(cmd, cmd->argv[1], "No such file or directory"));
	if (chdir_value == 1 && oldpwd)
		return (1);
	if (oldpwd && !ft_change_dir_update(sh->env, oldpwd))
		return (ft_sherror(cmd, cmd->argv[1], "No such file or directory"));
	cwd = ft_getcwd();
	if (!oldpwd && !cwd)
	{
		free(cwd);
		return (ft_cdparenterror(cmd));
	}
	free(cwd);
	return (0);
}

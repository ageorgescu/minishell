/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_signal.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 15:09:23 by algeorge          #+#    #+#             */
/*   Updated: 2022/12/14 15:09:23 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_heredoc_int(int signal)
{
	(void)signal;
	close(g_error_status);
	g_error_status = 130;
	write(1, "\n", 1);
	ft_close_stdfds();
	exit(130);
}

void	ft_save_stdfds(int *save_fd)
{
	save_fd[0] = dup(0);
	save_fd[1] = dup(1);
}

void	ft_restore_stdfds(int *save_fd)
{
	dup2close(&save_fd[0], 0);
	dup2close(&save_fd[1], 1);
}

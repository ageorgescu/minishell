/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_ali.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/11 16:30:40 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/04/12 16:56:19 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	free_wordlst(t_lst *lst)
{
	t_lst	*temp;

	while (lst)
	{
		temp = lst->next;
		delete_node(lst);
		lst = temp;
	}
}

int	error_print(char *error_msg, t_lst *lst, int signal)
{
	if (lst)
		free_wordlst(lst);
	ft_putstr_fd(error_msg, 2);
	return (signal);
}

int	check_quotes(char *str)
{
	int		i;
	char	flag;

	i = 0;
	flag = 0;
	while (str[i])
	{
		if (flag && flag == str[i])
			flag = 0;
		else
			if (!flag && ft_strchr("'\"", str[i]))
				flag = str[i];
		i++;
	}
	if (flag)
	{
		ft_putstr_fd("minishell: unexpected EOF while ", 2);
		ft_putstr_fd("looking for matching `", 2);
		ft_putchar_fd(flag, 2);
		ft_putstr_fd("'\n", 2);
	}
	return (flag);
}

int	parsing(t_shell *shell)
{
	if (check_quotes(shell->input))
		return (2);
	if (tokenize(shell->input, shell))
		return (error_print("Memory error\n", shell->tokens, 1));
	if (analyzer(shell))
		return (error_print("", shell->tokens, 2));
	if (expansions(shell, shell->env))
		return (error_print("Memory error\n", shell->tokens, 1));
	return (0);
}

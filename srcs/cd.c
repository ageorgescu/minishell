/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 11:09:13 by algeorge          #+#    #+#             */
/*   Updated: 2022/11/01 11:09:13 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_nogetcwd(t_list **env, char **newpwd, t_cmd *cmd)
{
	t_list	*env2;

	env2 = ft_search_env(*env, "PWD");
	if (env2 && (env2)->content && ((t_env *)((env2)->content))->key)
	{
		if (ft_strcmp(cmd->argv[1], ".") == 0)
			*newpwd = ft_strjoin(((t_env *)((env2)->content))->value, "/.");
		else if (ft_strcmp(cmd->argv[1], "..") == 0)
			*newpwd = ft_strjoin(((t_env *)((env2)->content))->value, "/..");
		else
			*newpwd = ft_strdup(((t_env *)((env2)->content))->value);
		if (!*newpwd)
			return ;
	}
}

int	ft_chdir_value(t_cmd *cmd, t_shell *sh)
{
	t_list	*home;

	if (!cmd->argv[1] || (!ft_strcmp(cmd->argv[1], "~") && !cmd->argv[2]))
	{
		home = ft_search_env(sh->env, "HOME");
		if (!home)
		{
			ft_sherror(cmd, cmd->argv[1], "HOME not set");
			return (1);
		}
		return (chdir(((t_env *)home->content)->value));
	}
	else
		return (chdir(cmd->argv[1]));
}

int	ft_cd(t_cmd *cmd, t_shell *sh)
{
	char	*oldpwd;
	char	*newpwd;

	if (cmd->argv[1] && cmd->argv[2])
		return (ft_error(cmd, "minishell: cd: too many arguments"));
	newpwd = NULL;
	oldpwd = ft_getcwd();
	if (!oldpwd && cmd->argv[1])
		ft_nogetcwd(&sh->env, &newpwd, cmd);
	if (ft_chdir(ft_chdir_value(cmd, sh), oldpwd, cmd, sh)
		|| (newpwd && !ft_change_dir_update(sh->env, newpwd)))
		return (ft_freetmppwd(&oldpwd, &newpwd));
	if (oldpwd)
		free(oldpwd);
	if (newpwd)
		free(newpwd);
	return (0);
}

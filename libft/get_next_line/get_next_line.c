/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aalkhiro <aalkhiro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 08:17:41 by aalkhiro          #+#    #+#             */
/*   Updated: 2023/03/17 13:43:16 by aalkhiro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*buff_ctrl(char *buffer, char *newln)
{
	int		nl;
	char	*temp;

	nl = findnl(buffer);
	temp = ft_gnl_substr(buffer, 0, nl + 1);
	if (!temp)
	{
		free(newln);
		return (NULL);
	}
	newln = ft_gnl_strjoin(newln, temp);
	free(temp);
	if (!newln)
		return (NULL);
	if ((size_t)nl == ft_gnl_strlen(buffer))
		buffer[0] = '\0';
	else
		ft_gnl_memmove(buffer, &buffer[nl + 1],
			ft_gnl_strlen(&buffer[nl + 1]) + 1);
	return (newln);
}

char	*get_line(char *buffer, char *newln, int fd)
{
	int		rt;

	while (1)
	{
		if (buffer[0] == '\0')
		{
			rt = read(fd, buffer, BUFFER_SIZE);
			if (rt <= 0)
			{
				if (newln[0] == '\0' || rt < 0)
				{
					free(newln);
					return (NULL);
				}
				else
					return (newln);
			}
			buffer[rt] = '\0';
		}
		newln = buff_ctrl(buffer, newln);
		if (!newln)
			return (NULL);
		if (newln[ft_gnl_strlen(newln) - 1] == '\n')
			return (newln);
	}
}

char	*get_next_line(int fd)
{
	static char	buff[BUFFER_SIZE + 1] = {0};
	char		*buffer;
	char		*newln;

	if (fd < 0 || BUFFER_SIZE <= 0)
		return (NULL);
	newln = malloc(sizeof(char));
	if (!newln)
	{
		write(2, "GNL Memory error\n", 17);
		return (NULL);
	}
	newln[0] = '\0';
	buffer = &buff[0];
	return (get_line(buffer, newln, fd));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/18 16:22:30 by algeorge          #+#    #+#             */
/*   Updated: 2022/10/18 16:22:30 by algeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include "libft.h"
# include <stdio.h>
# include <signal.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <readline/readline.h>
# include <readline/history.h>
# include <fcntl.h>

typedef struct s_lst
{
	char			*content;
	int				dis;
	struct s_lst	*next;
	struct s_lst	*prev;
}				t_lst;

typedef struct s_cmd
{
	char			**argv;
	t_lst			*redir;
	int				pid;
	int				status;
}				t_cmd;

typedef struct s_env
{
	char	*key;
	char	*value;
}				t_env;

typedef struct s_shell
{
	t_list	*env;
	int		nbline;
	int		sinfd;
	int		soutfd;
	int		save_fd[2];
	int		save_out;
	int		old_pipe_in;
	char	*input;
	t_lst	*tokens;
	t_cmd	*cmds;
	char	*status_str;
}				t_shell;

extern int	g_error_status;

# define TRUE 1
# define FALSE 0
# define OUTPUT 1
# define OUTPUT_APP 2
# define INPUT 3
# define HERE_DOC 4
# define PIPEX 6
# define QSTR "'\""
/*shell.c*/
int		ft_initshell(char **env, t_shell *sh);
void	ft_freeshell(t_shell *sh);
t_cmd	*ft_freecmd(t_cmd *cmd);

/*env_list.c */
t_list	*ft_create_env_list(char **env);
t_list	*ft_search_env(t_list *env, char *arg);
void	ft_lstclear_env(t_list **env);
void	ft_free_env_lst(void *env_lst);
t_env	*ft_new_env_var(char *key, char *value);

/*env_list2.c */
t_list	*ft_update_env(t_list *env, t_env *data, int add);
t_env	*ft_separate_key_value(char *str, int *add);

/*env_list3.c*/
char	**ft_lst_env_to_split_export(t_list *lst_env, int export);
char	*ft_get_env_value(t_list *lst_env, char *key, t_shell *sh);
int		get_path_var(char **path, t_shell *sh);

/*cd2.c*/
char	*ft_getcwd(void);
t_list	*ft_change_dir_update(t_list *env, char *oldpwd);
int		ft_freetmppwd(char **oldpwd, char **newpwd);
int		ft_cdparenterror(t_cmd *cmd);
int		ft_chdir(int chdir_value, char *oldpwd, t_cmd *cmd, t_shell *sh);

/*util.c*/
void	ft_safe_free(void **ptr);
int		ft_is_escaped(char *str, int pos);
void	ft_create_pipe(int *old_pipe_in, int next);
int		is_builtin(char *value);
void	ft_close_stdfds(void);
/*util2.c*/
int		ft_isalldigit(char *str);
int		ft_isblank(char *str);
int		add_path_cmd(t_cmd *cmd, char *cmdname,
			char **cmd_wpath, t_shell *sh);

/*errors.c*/
int		ft_error(t_cmd *cmd, char *error);
void	ft_enverror(t_cmd *cmd);
int		ft_sherror(t_cmd *cmd, char *arg, char *str);
void	ft_unseterror(t_cmd *cmd, char *arg);
void	ft_heredoc_warning(char *eof, int nbline, int tmp_fd, int old_pipe_in);

/*exec_path.c*/
char	*get_absolute_path(char *cmd, char *path_env);
int		is_executable(char *cmd_path);

/*signal.c & heredoc_signal.c*/
void	ft_exec_signals(void);
void	ft_input_signals(void);
void	ft_heredoc_int(int signal);
void	ft_save_stdfds(int *save_fd);
void	ft_restore_stdfds(int *save_fd);

/*prompt.c*/
int		ft_prompt(t_shell *sh);

/*exec.c*/
void	ft_exec(t_cmd *cmd, t_shell *sh);
int		ft_exec_builtin(t_cmd *cmd, t_shell *sh, int pipe);

int		ft_cd(t_cmd *cmd, t_shell *sh);
int		ft_echo(t_cmd *cmd);
int		ft_pwd(t_cmd *cmd, t_shell *sh);
int		ft_env(t_cmd *cmd, t_shell *sh);
int		ft_export(t_cmd *cmd, t_shell *sh);
int		ft_unset(t_cmd *cmd, t_shell *sh);
int		ft_exit(t_cmd *cmd, t_shell *sh, int pipe);

/*heredoc.c*/
int		ft_heredoc(t_cmd *cmd, char *eof, int flag, t_shell *sh);
void	make_tmp_file_input(void);
/*heredoc_expansion.c*/
int		ft_heredoc_expansion(int fd, t_list *env_lst, t_shell *sh, t_cmd *cmd);

/*LISTS*/
t_lst	*init_node(char *input, int size);
void	delete_node(t_lst *node);
t_lst	*unlink_node(t_lst *node);
void	add_node_front(t_lst *node, t_lst **lst);
void	add_node_back(t_lst *node, t_lst **lst);

/*parser*/
int		redirections(t_cmd *cmd, t_shell *shell);
int		tokenize(char *input, t_shell *shell);
int		expansions(t_shell *shell, t_list *env_list);
char	*expansion(char *str, t_list *env_list, t_shell *sh);
int		analyzer(t_shell *shell);
int		parsing(t_shell *sh);
int		error_print(char *error_msg, t_lst *lst, int signal);
int		reconstructor(t_shell *shell);
int		dup2close(int *source, int target);
int		err_out(t_cmd *cmd, char *msg1, char *msg2, char *msg3);
void	ft_trimmer(char *str);
void	remove_quotes(t_lst *lst);

#endif

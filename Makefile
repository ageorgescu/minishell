# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: algeorge <algeorge@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/18 16:38:51 by algeorge          #+#    #+#              #
#    Updated: 2022/10/18 16:38:51 by algeorge         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= minishell

SRCF	= 	main.c \
			shell.c \
			cd.c \
			cd2.c \
			echo.c \
			env.c \
			env_list.c \
			env_list2.c \
			env_list3.c \
			pwd.c \
			util.c \
			util2.c \
			errors.c \
			export.c \
			unset.c \
			signals.c \
			exit.c \
			exec.c \
			exec_builtin.c \
			exec_path.c \
			prompt.c \
			heredoc.c \
			heredoc_signal.c \
			heredoc_expansion.c \
			analizer.c \
			cmd_constr.c \
			expander.c \
			node_man_ali.c \
			parser_ali.c \
			redirections.c \
			tokenizer.c	\
			ft_trimmer.c
SRCS_DIR	= srcs
SRCS	= $(addprefix $(SRCS_DIR)/, $(SRCF))
OBJS	= $(SRCS:.c=.o)

CC				= gcc
READLINE		= -lreadline
HEADER			= -I ./includes $(LIBFT_INCL)
CFLAGS			= -Wall -Wextra -Werror -g $(HEADER)

LIBFT_PATH		= ./libft
LIBFT_MAKE		= make -C $(LIBFT_PATH) --no-print-directory
LIBFT_INCL		= -I $(LIBFT_PATH)
LIBFT			= $(LIBFT_PATH)/libft.a

all:	libftmake
		make $(NAME) --no-print-directory

libftmake:
			$(LIBFT_MAKE)


$(NAME):	$(OBJS)
			$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LIBFT) $(READLINE)

clean:
				make clean -C ./libft
				rm -rf $(OBJS)

fclean:			clean
				make fclean -C ./libft
				rm -rf $(NAME)

re:				fclean all